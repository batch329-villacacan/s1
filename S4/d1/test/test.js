const { factorial, isDivisibleBy5or7 } = require('../src/util.js');

// gets the expect and assert functions from chai to be used
const { expect, assert } = require('chai');

// Test suites are made of collection of test cases that should be executed together

// "describe()"" keyword is used to group tests together
describe('test_fun_factorials', () => {

    // "it()" is used to define a single test case
    // "it()"" accepts two parameters
    // a string explaining what the test should do
    //  callback function which contain the actual test

    // "it("string",what to test)

    it('test_fun_factorial_5!_is_120', () => {
        const product = factorial(5);
        expect(product).to.equal(120);
    })

    it('test_fun_factorial_1!_is_1', () => {
        const product = factorial(1);
        assert.equal(product, 1);
    })

    // ACTIVITY

    // Check if 0! is 1
    it('test_fun_factorial_0!_is_1', () => {
        const product = factorial(0);
        expect(product).to.equal(1);
    })

    // Check if 4! is 24
    it('test_fun_factorial_4!_is_24', () => {
        const product = factorial(4);
        assert.equal(product, 24);
    })

    // Check if 10! is 3628800
    it('test_fun_factorial_10!_is_3628800', () => {
        const product = factorial(10);
        expect(product).to.equal(3628800);
    })

    // Test for negative numbers
    // RED - write a test that fails
    it('test_fun_factorials_neg1_is_undefined', () => {
        // refactor add factorial function
        const product = factorial(-1
        );
        expect(product).to.equal(undefined);
    })

    it("test_fun_factorials_invalid_number_is_undefined", ()=> {
        const product = factorial('abc');
        expect(product).to.equal(undefined);
    })

})

// ACTIVITY: Test divisibility by 5 or 7

describe('test_divisibility_by_5_or_7',() => {
    it('test_100_is_divisible_by_5', () => {
        const isDivisible = isDivisibleBy5or7(125);
        expect(isDivisible).to.equal(true);
    })

    it('test_100_is_divisible_by_7', () => {
        const isDivisible = isDivisibleBy5or7(91);
        assert.equal(isDivisible, true);
    })

    it('test_100_is_divisible_by_5', () => {
        const isDivisible = isDivisibleBy5or7(90);
        expect(isDivisible).to.equal(true);
    })

    it('test_100_is_divisible_by_7', () => {
        const isDivisible = isDivisibleBy5or7(392);
        assert.equal(isDivisible, true);
    })
})

