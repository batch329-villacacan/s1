function factorial(n){
    // if(typeof n !== 'number') return undefined; // will use in s04
    if(n<0) return undefined; // will use in s04
    if(n===0) return 1;
    if(n===1) return 1;
    if(typeof n !== 'number') return undefined;
    return n * factorial(n-1);
}

function isDivisibleBy5or7(n){
    if(n%5===0) return true;
    if(n%7===0) return true;
    return false;
}

const names = {
    "Jace" : {
        "name" : "Jace Theodore Villacacan",
        "age": 2
    },
    "Princess" : {
        "name" : "Princess Fajardo",
        "age" : 25
    }
}

// S4 Activity Template START
const users = [
    {
        username: "brBoyd87",
        password: "87brandon19"

    },
    {
        username: "tylerofsteve",
        password: "stevenstyle75"
    }
]
// S4 Activity Template END

module.exports = {
    factorial: factorial,
    isDivisibleBy5or7: isDivisibleBy5or7,
    names: names,
    users: users
}



