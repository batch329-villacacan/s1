const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})

	// 1 Check if post /currency is running
	it("test post currency if api is running", () => {
        chai.request("http://localhost:5001")
        .post('/currency')
        .end((err, res) => {
            expect(res).to.not.equal(undefined);
        })
    })

	it('test post currency api if name is missing', (done) => {
		chai.request("http://localhost:5001")
		.post('/currency')
		.type('json')
		.send({
			'won': {
				'ex': {
				  'peso': 0.043,
				  'usd': 0.00084,
				  'yen': 0.092,
				  'yuan': 0.0059
				}
			}
		})
        .end((err, res) => {
            expect(res.status).to.equal(400)
			done()
        })
	})

	it('test post currency api if name is not a string', (done) => {
		chai.request("http://localhost:5001")
		.post("/currency")
		.type('json')
		.send({
			'won': {
				'name' : 1234,
				'ex': {
				  'peso': 0.043,
				  'usd': 0.00084,
				  'yen': 0.092,
				  'yuan': 0.0059
				}
			}
		})
        .end((err, res) => {
            expect(res.status).to.equal(400)
			done()
        })
	})

	it('test post currency api if name is empty', (done) => {
		chai.request("http://localhost:5001")
		.post("/currency")
		.type('json')
		.send({
			'won': {
				'name' : '',
				'ex': {
				  'peso': 0.043,
				  'usd': 0.00084,
				  'yen': 0.092,
				  'yuan': 0.0059
				}
			}
		})
        .end((err, res) => {
            expect(res.status).to.equal(400)
			done()
        })
	})

	it('test post currency api if ex is missing', (done) => {
		chai.request("http://localhost:5001")
		.post("/currency")
		.type('json')
		.send({
			'won': {
				'name' : 'South Korean Won',
			}
		})
        .end((err, res) => {
            expect(res.status).to.equal(400)
			done()
        })
	})

	it('test post currency api if ex is not an object', (done) => {
		chai.request("http://localhost:5001")
		.post("/currency")
		.type('json')
		.send({
			'won': 'South Korean Won',
		})
        .end((err, res) => {
            expect(res.status).to.equal(400)
			done()
        })
	})

	it('test post currency api if ex is empty', (done) => {
		chai.request("http://localhost:5001")
		.post("/currency")
		.type('json')
		.send({
			name: "usd",
		})
        .end((err, res) => {
            expect(res.status).to.equal(400)
			done()
        })
	})

	it('test post currency api if alias is missing', (done) => {
		chai.request("http://localhost:5001")
		.post("/currency")
		.type('json')
		.send({
			'ex': {
				'peso': 7.21,
				'usd': 0.14,
				'won': 168.85,
				'yen': 15.45
			  }
		})
        .end((err, res) => {
            expect(res.status).to.equal(400)
			done()
        })
	})

	it('test post currency api if alias is not a string', (done) => {
		chai.request("http://localhost:5001")
		.post("/currency")
		.type('json')
		.send({
			'peso': {
				'name': 'Philippine Peso',
				'ex':{
				  'usd': 0.020,
				  'won': 23.39,
				  'yen': 2.14,
				  'yuan': 0.14
				}
				,
		  'alias':54
		  }
		})
        .end((err, res) => {
            expect(res.status).to.equal(400)
			done()
        })
	})

	it('test post currency api if alias is empty', (done) => {
		chai.request("http://localhost:5001")
		.post("/currency")
		.type('json')
		.send({
			'peso': {
				'name': 'Philippine Peso',
				'ex':{
				  'usd': 0.020,
				  'won': 23.39,
				  'yen': 2.14,
				  'yuan': 0.14
				}
				,
		  'alias':''
		  }
		})
        .end((err, res) => {
            expect(res.status).to.equal(400)
			done()
        })
	})

	it('test post currency api if  all fields are complete but there is a duplicate alias', (done) => {
		chai.request("http://localhost:5001")
		.post("/currency")
		.type('json')
		.send({
			name: "USD",
            alias: "US Dollar",
            ex: {
                euro: 0.85
            }
		})
        .end((err, res) => {
            expect(res.status).to.equal(200)
			done()
        })
	})

	it("test_api_post_currency_returns_200_if_all_fields_complete_no_duplicates", (done) => {
        chai.request("http://localhost:5001")
        .post("/currency")
        .type("json")
        .send({
            name: "USD",
            alias: "US Dollar",
            ex: {
                euro: 0.85
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(200)
            done();
        })
    })












	
	

})
