const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})
	
	app.post('/currency', (req, res) => {

		// Check if post /currency returns status 400 if name is missing
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter NAME'
			})
		}

		//  Check if post /currency returns status 400 if name is not a string
		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - NAME should be a STRING'
			})
		}

		// Check if post /currency returns status 400 if name is empty
		if(req.body.name === ''){
			return res.status(400).send({
				'error': 'Bad Request - NAME should not be a EMPTY'
			})
		}

		// Check if post /currency returns status 400 if ex is missing
		if(!req.body.ex){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter EX'
			})
		}

		// Check if post /currency returns status 400 if ex is not an object
		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				'error': 'Bad Request - EX should be an OBJECT'
			})
		}

		// Check if post /currency returns status 400 if ex is empty
		if(!req.body.ex === ''){
			return res.status(400).send({
				'error': 'Bad Request - EX should not be empty'
			})
		}

		// Check if post /currency returns status 400 if alias is missing
		if(!req.body.alias){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter EX'
			})
		}

		// Check if post /currency returns status 400 if alias is not an string
		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter EX'
			})
		}

		// Check if post /currency returns status 400 if alias is empty
		if(!req.body.alias === ''){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter EX'
			})
		}

		// Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
		if(!req.body.alias === req.body.name){
			return res.status(400).send({
				'error': 'Bad Request - Duplicate alias found'
			})
		}

		const alias = req.body.alias;
		const existingCurrencies = Object.values(exchangeRates);
		const duplicateAlias = existingCurrencies.some(currency => currency.alias === alias)
		if (duplicateAlias) {
			return res.status(400).send({
				'error': 'Bad Request - Duplicate alias found'
			})
		}

   
		return res.status(200).send({
			'message': 'Currency data received successfully'
		})

	// end of app.post
	})
}
