function factorial(n){
    // if(typeof n !== 'number') return undefined; // will use in s04
    // if(n<0) return undefined; // will use in s04
    if(n===0) return 1;
    if(n===1) return 1;
    return n * factorial(n-1);
}

function isDivisibleBy5or7(n){
    if(n%5===0) return true;
    if(n%7===0) return true;
    return false;
}

const names = {
    "Jace" : {
        "name" : "Jace Theodore Villacacan",
        "age": 2
    },
    "Princess" : {
        "name" : "Princess Fajardo",
        "age" : 25
    }
}

module.exports = {
    factorial : factorial,
    isDivisibleBy5or7 : isDivisibleBy5or7,
    names : names
}


